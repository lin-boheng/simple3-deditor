#ifndef __EDITOR_DIALOG_TIPS__
#define __EDITOR_DIALOG_TIPS__

#include <define.h>

void DialogTextInput(wchar_t* str, size_t size);
void DialogVersionInfo();

#endif